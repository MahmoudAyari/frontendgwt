package fr.istic.taa.gwt.client;

import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;

import fr.istic.taa.gwt.shared.IProject;
import fr.istic.taa.gwt.shared.ISprint;
import fr.istic.taa.gwt.shared.ITask;

public class DialogUpdateTask extends DialogBox {

	Agile view;
    ISprint sprint;
	public DialogUpdateTask(final ITask task ,Agile agile,  final ISprint sprint) {
		
		this.view = agile;
		this.sprint = sprint;

		/* Title of the dialog box */
		setText("Mise à jour d'une tâche");

		setAnimationEnabled(true);
		setGlassEnabled(true);
		setAutoHideEnabled(true);

		/* Name field */
		Label nameLabel = new Label("Nom");
		final TextBox tName = new TextBox();
		tName.setWidth("120px");
		tName.setText(task.getName());
		HorizontalPanel namePanel = new HorizontalPanel();
		namePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		namePanel.setSpacing(3);
		namePanel.add(nameLabel);
		namePanel.add(tName);

		/* Date start fields */
		
		Label datedebutLabel = new Label("Date début");		
		final DateBox datedebut = new DateBox();
		datedebut.setWidth("100px");
		datedebut.setValue(task.getStartDate());
		HorizontalPanel datedebutPanel = new HorizontalPanel();
		datedebutPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		datedebutPanel.setSpacing(3);
		datedebutPanel.add(datedebutLabel);
		datedebutPanel.add(datedebut);
		/*Date end field*/
		Label datefinLabel = new Label("Date fin");		
		final DateBox datefin = new DateBox();
		datefin.setWidth("100px");
		datefin.setValue(task.getDeadline());
		HorizontalPanel datefinPanel = new HorizontalPanel();
		datefinPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		datefinPanel.setSpacing(3);
		datefinPanel.add(datefinLabel);
		datefinPanel.add(datefin);

		/* Comment field */
		Label commentLabel = new Label("Commentaire");
		final TextArea tComment = new TextArea();
		tComment.setText(task.getComment());
		tComment.setWidth("150px");
		tComment.setHeight("45px");
		HorizontalPanel commentPanel = new HorizontalPanel();
		commentPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		commentPanel.setSpacing(3);
		commentPanel.add(commentLabel);
		commentPanel.add(tComment);
		
		Label etatLabel = new Label("Etat");
		final ListBox tEtat = new ListBox();
		
		tEtat.addItem("En cours");
		tEtat.addItem("Terminée");
		tEtat.addItem("Annulée");
		
		HorizontalPanel etatPanel = new HorizontalPanel();
		etatPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		etatPanel.setSpacing(3);
		etatPanel.add(etatLabel);
		etatPanel.add(tEtat);
		/* Add all in a vertical panel */
		VerticalPanel panel = new VerticalPanel();
		panel.setHeight("100");
		panel.setWidth("300");
		panel.setSpacing(10);
		panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		panel.add(namePanel);
		panel.add(datedebutPanel);
		panel.add(datefinPanel);
		panel.add(commentPanel);
		panel.add(etatPanel);

		
		

		/* Add button */
		Button valider = new Button("Valider");
		valider.setStyleName("btn");
		valider.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String taskName = tName.getText().trim();
				Date taskDateDebut = datedebut.getValue();
				Date taskDateFin = datefin.getValue();
				String taskComment = tComment.getText().trim();	
				int taskEtat = tEtat.getSelectedIndex();
				String etat="";
				switch (taskEtat) {
				case 0:
					etat= "En cours";
					break;
				case 1 :
					  etat= "Terminée";
					break;
				case 2:
					  etat= "Annulée";
					break;

				default:
					break;
				}
				if (taskName == "") {
					Window.alert("Entrez un nom");
				} else if (taskDateDebut == null) {
					Window.alert("Entrez une date début");
				} else if (taskDateFin == null) {
					Window.alert("Entrez une date fin");
				} else if (taskComment == "") {
					Window.alert("Entrez un commentaire ");
				} 
				else if (etat == "") {
					Window.alert("Entrez un commentaire ");
				}
				else {
					 view.updateTask(task.getId(),taskName, taskDateDebut, taskDateFin,taskComment,etat, sprint);
					DialogUpdateTask.this.hide();
				}          
			}
		});
        /* Cancel Button*/
		Button annuler = new Button("Annuler");
		annuler.setStyleName("btn");
		annuler.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				DialogUpdateTask.this.hide();      
			}
		});
		 HorizontalPanel hp = new HorizontalPanel();
		    hp.setCellHorizontalAlignment(valider, HasHorizontalAlignment.ALIGN_RIGHT);
		    hp.setCellHorizontalAlignment(annuler, HasHorizontalAlignment.ALIGN_RIGHT);
		    hp.add(valider);
		    hp.add(annuler); 
		
		
		

		/* Add validate button */
		panel.add(hp);

		setWidget(panel);
	}



}
