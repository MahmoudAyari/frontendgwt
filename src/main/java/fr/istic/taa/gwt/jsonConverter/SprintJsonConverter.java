package fr.istic.taa.gwt.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.taa.gwt.shared.ISprint;
import fr.istic.taa.gwt.shared.JsonFactory;






public class SprintJsonConverter {

	private SprintJsonConverter() {

	}

	private static SprintJsonConverter instance = new SprintJsonConverter();


	// Instantiate the factory
	JsonFactory factory = GWT.create(JsonFactory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public ISprint makeSprint() {
		// Construct the AutoBean
		AutoBean<ISprint> sprint = factory.sprint();

		// Return the Book interface shim
		return sprint.as();
	}

	public String serializeToJson(ISprint sprint) {
		// Retrieve the AutoBean controller
		AutoBean<ISprint> bean = AutoBeanUtils.getAutoBean(sprint);

		return AutoBeanCodex.encode(bean).getPayload();
	}

	public ISprint deserializeFromJson(String json) {
		AutoBean<ISprint> bean = AutoBeanCodex.decode(factory, ISprint.class, json);
		return bean.as();
	}

	public static SprintJsonConverter getInstance() {
		return instance;
	}
}
