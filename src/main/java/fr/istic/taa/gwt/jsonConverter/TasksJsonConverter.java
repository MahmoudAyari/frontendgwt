package fr.istic.taa.gwt.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.taa.gwt.shared.ISprints;
import fr.istic.taa.gwt.shared.ITasks;
import fr.istic.taa.gwt.shared.JsonFactory;



public class TasksJsonConverter {

	private TasksJsonConverter() {

	}

	private static TasksJsonConverter instance = new TasksJsonConverter();


	// Instantiate the factory
	JsonFactory factory = GWT.create(JsonFactory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public ITasks makeProjects() {
		// Construct the AutoBean
		AutoBean<ITasks> tasks = factory.tasks();

		// Return the Book interface shim
		return tasks.as();
	}

	String serializeToJson(ITasks tasks) {
		// Retrieve the AutoBean controller
		AutoBean<ITasks> bean = AutoBeanUtils.getAutoBean(tasks);

		return AutoBeanCodex.encode(bean).getPayload();
	}

	public ITasks deserializeFromJson(String json) {
		AutoBean<ITasks> bean = AutoBeanCodex.decode(factory, ITasks.class,"{\"tasks\":" + json + "}");
		return bean.as();
	}

	public static TasksJsonConverter getInstance() {
		return instance;
	}
}
