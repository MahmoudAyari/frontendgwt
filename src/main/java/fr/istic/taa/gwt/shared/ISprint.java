package fr.istic.taa.gwt.shared;

import java.util.Date;

public interface ISprint {

	
	  
	    
	    public int getId();

		public void setId(int id);

		public String getName();

		public void setName(String name);

		public Date getStartDate();

		public void setStartDate(Date date);

		public Date getEndDate();

		public void setEndDate(Date date);

		public String getComment();

		public void setComment(String comment);
		
		public IProject getProject();

		public void setProject(IProject project);
	
}
