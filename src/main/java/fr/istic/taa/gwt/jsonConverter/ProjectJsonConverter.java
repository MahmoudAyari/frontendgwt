package fr.istic.taa.gwt.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.taa.gwt.shared.IProject;
import fr.istic.taa.gwt.shared.JsonFactory;






public class ProjectJsonConverter {

	private ProjectJsonConverter() {

	}

	private static ProjectJsonConverter instance = new ProjectJsonConverter();


	// Instantiate the factory
	JsonFactory factory = GWT.create(JsonFactory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public IProject makeProject() {
		// Construct the AutoBean
		AutoBean<IProject> project = factory.project();

		// Return the Book interface shim
		return project.as();
	}

	public String serializeToJson(IProject project) {
		// Retrieve the AutoBean controller
		AutoBean<IProject> bean = AutoBeanUtils.getAutoBean(project);

		return AutoBeanCodex.encode(bean).getPayload();
	}

	public IProject deserializeFromJson(String json) {
		AutoBean<IProject> bean = AutoBeanCodex.decode(factory, IProject.class, json);
		return bean.as();
	}

	public static ProjectJsonConverter getInstance() {
		return instance;
	}
}
