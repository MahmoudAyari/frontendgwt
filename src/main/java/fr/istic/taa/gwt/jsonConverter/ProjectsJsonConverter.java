package fr.istic.taa.gwt.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.taa.gwt.shared.IProjects;
import fr.istic.taa.gwt.shared.JsonFactory;



public class ProjectsJsonConverter {

	private ProjectsJsonConverter() {

	}

	private static ProjectsJsonConverter instance = new ProjectsJsonConverter();


	// Instantiate the factory
	JsonFactory factory = GWT.create(JsonFactory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public IProjects makeProjects() {
		// Construct the AutoBean
		AutoBean<IProjects> projects = factory.projects();

		// Return the Book interface shim
		return projects.as();
	}

	String serializeToJson(IProjects projects) {
		// Retrieve the AutoBean controller
		AutoBean<IProjects> bean = AutoBeanUtils.getAutoBean(projects);

		return AutoBeanCodex.encode(bean).getPayload();
	}

	public IProjects deserializeFromJson(String json) {
		AutoBean<IProjects> bean = AutoBeanCodex.decode(factory, IProjects.class,"{\"projects\":" + json + "}");
		return bean.as();
	}

	public static ProjectsJsonConverter getInstance() {
		return instance;
	}
}
