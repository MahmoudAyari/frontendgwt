package fr.istic.taa.gwt.shared;

import java.util.Date;



public interface IProject {

	public int getId();

	public void setId(int id);

	public String getName();

	public void setName(String name);

	public Date getStartDate();

	public void setStartDate(Date date);

	public Date getEndDate();

	public void setEndDate(Date date);

	public String getDescription();

	public void setDescription(String description);

}
