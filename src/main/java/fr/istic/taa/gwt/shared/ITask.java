package fr.istic.taa.gwt.shared;

import java.util.Date;

public interface ITask {

	
	  
	    public int getId();

		public void setId(int id);

		public String getName();

		public void setName(String name);

		public Date getStartDate();

		public void setStartDate(Date date);

		public Date getDeadline();

		public void setDeadline(Date date);

		public String getComment();

		public void setComment(String comment);
		
		public String getEtat();

		public void setEtat(String etat);
		
		public ISprint getSprint();
		
		public void setSprint(ISprint sprint);
}
