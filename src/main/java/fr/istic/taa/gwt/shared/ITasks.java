package fr.istic.taa.gwt.shared;

import java.util.List;

public interface ITasks {
	
    void setTasks(List<ITask> tasks);
	
	List<ITask> getTasks();
}
