package fr.istic.taa.gwt.client;


import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.istic.taa.gwt.shared.ISprint;
import fr.istic.taa.gwt.shared.ITask;

/**
 * Renders a Cell which represents an task, in the tasks list
 */
public class TaskCell extends AbstractCell<ITask> {

	/**
	 * The HTML templates used to render a Cell representing an task
	 */
	interface Templates extends SafeHtmlTemplates {		
		@SafeHtmlTemplates.Template("<div>"
								  + 	"<span class='project-name'>{0}</span> - <span>{1}</span>"
								  + "</div>"
								  + "<div class='project-desc'>{2}</div>")
		SafeHtml makeTaskCell(String name, String dateStart, String comment);		
	}

	/**
	 * Create a singleton instance of the templates used to render the cell.
	 */
	private static Templates templates = GWT.create(Templates.class);

	@Override
	public void render(Context context, ITask value, SafeHtmlBuilder sb) {
		if (value != null) {
			
			String dateStart = DateTimeFormat.getFormat("dd/MM/yyyy").format(value.getStartDate());
			String dateEnd = DateTimeFormat.getFormat("dd/MM/yyyy").format(value.getDeadline());
			SafeHtml project_cell_rendered = templates.makeTaskCell(value.getName(), dateStart, value.getComment());
			sb.append(project_cell_rendered);
		}		
	}

	
}