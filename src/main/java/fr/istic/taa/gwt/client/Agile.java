package fr.istic.taa.gwt.client;

import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import fr.istic.taa.gwt.jsonConverter.ProjectJsonConverter;
import fr.istic.taa.gwt.jsonConverter.ProjectsJsonConverter;
import fr.istic.taa.gwt.jsonConverter.SprintJsonConverter;
import fr.istic.taa.gwt.jsonConverter.SprintsJsonConverter;
import fr.istic.taa.gwt.jsonConverter.TaskJsonConverter;
import fr.istic.taa.gwt.jsonConverter.TasksJsonConverter;
import fr.istic.taa.gwt.shared.IProject;
import fr.istic.taa.gwt.shared.IProjects;
import fr.istic.taa.gwt.shared.ISprint;
import fr.istic.taa.gwt.shared.ISprints;
import fr.istic.taa.gwt.shared.ITask;
import fr.istic.taa.gwt.shared.ITasks;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Agile implements EntryPoint {

	private static List<IProject> PROJECTS;
	private static final String URL_REST = "http://localhost/";

	private CellList<IProject> projectsList;

	private LayoutPanel layout_left, layout_right;

	private IProject selectedProject;
	private ISprint selectedSprint;
	private ITask selectedTask;
	private CellList<ISprint> sprintsList;

	Button addTaskToSprintButton;
	private CellList<ITask> tasksList;

	private Button addProjectButton, updateTaskToSprintButton,updateSprintButton;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		RootPanel rootPanel = RootPanel.get();

		LayoutPanel layoutPanel = new LayoutPanel();
		layoutPanel.setSize("100%", "600px");

		// Create the left panel (project list)
		layout_left = new LayoutPanel();
		layout_left.setStyleName("left");
		layoutPanel.add(layout_left);
		layoutPanel.setWidgetTopHeight(layout_left, 0.0, Unit.PX, 100.0, Unit.PCT);

		// ScrollPanel for the projacts list
		ScrollPanel eventsScrollPanel = new ScrollPanel();
		layout_left.add(eventsScrollPanel);
		layout_left.setWidgetTopHeight(eventsScrollPanel, 0.0, Unit.PX, 550, Unit.PX);
		layout_left.setWidgetLeftWidth(eventsScrollPanel, 0.0, Unit.PX, 100, Unit.PCT);

		ProvidesKey<IProject> projectKeyProvider = new ProvidesKey<IProject>() {
			public Object getKey(IProject item) {
				return (item == null) ? null : item.getId();
			}
		};
		// Create a CellList using the keyProvider, for the projects list
		projectsList = new CellList<IProject>(new ProjectCell(), projectKeyProvider);
		projectsList.setWidth("25%");
		final SingleSelectionModel<IProject> selectionModel = new SingleSelectionModel<IProject>();
		projectsList.setSelectionModel(selectionModel);
		projectsList.addStyleName("left");
		eventsScrollPanel.add(projectsList);

		// Create  add project Button

		addProjectButton = new Button("Ajouter Projet");
		addProjectButton.setStyleName("btn");
		layout_left.add(addProjectButton);
		layout_left.setWidgetLeftWidth(addProjectButton, 3, Unit.PX, 100, Unit.PCT);
		layout_left.setWidgetTopHeight(addProjectButton, 570, Unit.PX, 25, Unit.PX);
		layout_left.setWidgetLeftWidth(addProjectButton, 131, Unit.PX, 100, Unit.PCT);
		layout_left.setWidgetTopHeight(addProjectButton, 570, Unit.PX, 25, Unit.PX);

		addProjectButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				DialogAddProject dialog = new DialogAddProject(Agile.this);
				dialog.center();
			}
		});
		// Create the right panel (project detail)
		layout_right = new LayoutPanel();
		layout_right.setStyleName("right");
		layoutPanel.add(layout_right);
		layoutPanel.setWidgetLeftWidth(layout_right, 25.0, Unit.PCT, 75.0, Unit.PCT);
		layoutPanel.setWidgetTopHeight(layout_right, 0.0, Unit.PX, 100.0, Unit.PCT);

		Label project_title = new Label();
		project_title.setAutoHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		project_title.setStyleName("project-detail-title");
		layout_right.add(project_title);
		layout_right.setVisible(false);
		layout_right.setWidgetLeftRight(project_title, 0, Unit.PX, 0, Unit.PX);

		// Two 50% panels for sprints list and tasks list
		LayoutPanel layout_sprints = new LayoutPanel(); 
		LayoutPanel layout_tasks = new LayoutPanel(); 
		layout_right.add(layout_sprints);
		layout_right.add(layout_tasks);
		layout_right.setWidgetTopHeight(layout_sprints, 35, Unit.PX, 560, Unit.PX);
		layout_right.setWidgetTopHeight(layout_tasks, 35, Unit.PX, 560, Unit.PX);
		layout_right.setWidgetLeftWidth(layout_sprints, 0, Unit.PX, 50.0, Unit.PCT);
		layout_right.setWidgetLeftWidth(layout_tasks, 50, Unit.PCT, 50.0, Unit.PCT);

		Label project_sprints_label = new Label("Sprints");
		project_sprints_label.setStyleName("project-detail-sprint-label");
		project_sprints_label.setAutoHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		layout_sprints.add(project_sprints_label);

		Label sprint_tasks_label = new Label("Tâches");
		sprint_tasks_label.setStyleName("project-detail-task-label");
		sprint_tasks_label.setAutoHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		layout_tasks.add(sprint_tasks_label);

		// Two scrollPanels for sprints and tasks lists
		ScrollPanel sprintsScrollPanel = new ScrollPanel();
		layout_sprints.add(sprintsScrollPanel);
		ScrollPanel tasksScrollPanel = new ScrollPanel();
		layout_tasks.add(tasksScrollPanel);

		Button addSprintButton = new Button("Ajout sprint");
		addSprintButton.setStyleName("btn");
		layout_sprints.add(addSprintButton);
		layout_sprints.setWidgetLeftWidth(addSprintButton, 125, Unit.PX, 100, Unit.PCT);
		layout_sprints.setWidgetTopHeight(addSprintButton, 535, Unit.PX, 25, Unit.PX);
		addSprintButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				DialogAddSprint dialog = new DialogAddSprint(Agile.this, selectedProject);
				dialog.center();
			}
		});
		// KeyProvider for the sprint of the selected project
		ProvidesKey<ISprint> usersKeyProvider = new ProvidesKey<ISprint>() {
			public Object getKey(ISprint item) {
				return (item == null) ? null : item.getId();
			}
		};
		// Create a CellList using the keyProvider, for the sprint list
		sprintsList = new CellList<ISprint>(new SprintCell(), usersKeyProvider);
		sprintsList.setStyleName("sprint-list");
		final SingleSelectionModel<ISprint> selectionModelSprints = new SingleSelectionModel<ISprint>();
		sprintsList.setSelectionModel(selectionModelSprints);
		sprintsScrollPanel.add(sprintsList);
		layout_sprints.setWidgetTopHeight(sprintsScrollPanel, 25, Unit.PX, 500, Unit.PX);

		ProvidesKey<ITask> carsKeyProvider = new ProvidesKey<ITask>() {
			public Object getKey(ITask item) {
				return (item == null) ? null : item.getId();
			}
		};
		// Create a CellList using the keyProvider, for the task list
		tasksList = new CellList<ITask>(new TaskCell(), carsKeyProvider);
		final SingleSelectionModel<ITask> selectionModelTasks = new SingleSelectionModel<ITask>();
		tasksList.setSelectionModel(selectionModelTasks);
		tasksList.setStyleName("task-list");
		tasksScrollPanel.add(tasksList);
		layout_tasks.setWidgetTopHeight(tasksScrollPanel, 25, Unit.PX, 500, Unit.PX);

		// BUTTON Add task to the sprint
		addTaskToSprintButton = new Button("Ajout tâche");
		addTaskToSprintButton.setStyleName("btn");
		addTaskToSprintButton.setVisible(false);
		layout_tasks.add(addTaskToSprintButton);
		layout_tasks.setWidgetLeftWidth(addTaskToSprintButton, 250, Unit.PX, 100, Unit.PCT);
		layout_tasks.setWidgetTopHeight(addTaskToSprintButton, 535, Unit.PX, 25, Unit.PX);
		addTaskToSprintButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				DialogAddTask dialog = new DialogAddTask(Agile.this, selectedSprint);
				dialog.center();
			}
		});

		// BUTTON update task
		updateTaskToSprintButton = new Button("Update tâche");
		updateTaskToSprintButton.setStyleName("btn");
		updateTaskToSprintButton.setVisible(false);
		layout_tasks.add(updateTaskToSprintButton);
		layout_tasks.setWidgetLeftWidth(updateTaskToSprintButton, 360, Unit.PX, 100, Unit.PCT);
		layout_tasks.setWidgetTopHeight(updateTaskToSprintButton, 535, Unit.PX, 25, Unit.PX);
		updateTaskToSprintButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				DialogUpdateTask dialog = new DialogUpdateTask(selectedTask, Agile.this, selectedSprint);
				dialog.center();
			}
		});
 
		// BUTTON update sprint
		updateSprintButton = new Button("Update sprint");
		updateSprintButton.setStyleName("btn");
		updateSprintButton.setVisible(false);
		layout_sprints.add(updateSprintButton);
		layout_sprints.setWidgetLeftWidth(updateSprintButton, 225, Unit.PX, 100, Unit.PCT);
		layout_sprints.setWidgetTopHeight(updateSprintButton, 535, Unit.PX, 25, Unit.PX);
		updateSprintButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				DialogUpdateSprint dialog = new DialogUpdateSprint(selectedSprint, Agile.this, selectedProject);
				dialog.center(); 
			}
		});
		
		rootPanel.add(layoutPanel);

		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				selectedProject = selectionModel.getSelectedObject();
				if (selectedProject != null) {
					((Label) layout_right.getWidget(0)).setText("Projet " + selectedProject.getName());
					layout_right.setVisible(true);
					loadProjectSprints(selectedProject);
				}
			}

		});

		selectionModelTasks.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				selectedTask = selectionModelTasks.getSelectedObject();
				updateTaskToSprintButton.setVisible(true);
			}

		});

		selectionModelSprints.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				selectedSprint = selectionModelSprints.getSelectedObject();
				if (selectedSprint != null) {

					loadSprintTasks(selectedSprint);
					addTaskToSprintButton.setVisible(true);
					updateSprintButton.setVisible(true);
				}
			}

		});

		loadAllProjects();

	}

	/**
	 * Launch an HTTP request to load all of the events, and if success fills
	 * the project list
	 */
	private void loadAllProjects() {

		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, URL.encode(URL_REST + "projects/all"));
		rb.setCallback(new RequestCallback() {

			public void onError(Request req, Throwable th) {
				Window.alert("Error while retrieving the list of projects");
			}

			public void onResponseReceived(Request req, Response res) {

				if (res.getStatusCode() == 200) {

					IProjects e = ProjectsJsonConverter.getInstance().deserializeFromJson(res.getText());
					// Window.alert(res.getText());
					projectsList.setRowCount(e.getProjects().size(), true);
					projectsList.setRowData(0, e.getProjects());
					// Window.alert(String.valueOf(e.getProjects().size()));

				}
			}

		});

		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while retrieving the list of projects(");
		}
	}

	private void loadProjectSprints(IProject project) {

		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
				URL.encode(URL_REST + "sprints/project/" + project.getId()));
		rb.setCallback(new RequestCallback() {

			public void onError(Request req, Throwable th) {
				Window.alert("Error while retrieving the list of sprint");
			}

			public void onResponseReceived(Request req, Response res) {

				if (res.getStatusCode() == 200) {

					ISprints e = SprintsJsonConverter.getInstance().deserializeFromJson(res.getText());
					sprintsList.setRowCount(e.getSprints().size(), true);
					sprintsList.setRowData(0, e.getSprints());

				}
			}

		});

		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while retrieving the list of sprints");
		}
	}

	private void loadSprintTasks(ISprint sprint) {

		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
				URL.encode(URL_REST + "tasks/sprint/" + sprint.getId()));
		rb.setCallback(new RequestCallback() {

			public void onError(Request req, Throwable th) {
				Window.alert("Error while retrieving the list of tasks");
			}

			public void onResponseReceived(Request req, Response res) {

				if (res.getStatusCode() == 200) {

					ITasks e = TasksJsonConverter.getInstance().deserializeFromJson(res.getText());
					tasksList.setRowCount(e.getTasks().size(), true);
					tasksList.setRowData(0, e.getTasks());

				}
			}

		});

		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while retrieving the list of tasks");
		}
	}

	protected void addProject(String projectName, Date projectDateDebut, Date projectDateFin, String projectDesc) {

		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, URL.encode(URL_REST + "projects/add"));
		rb.setHeader("Content-Type", "application/json");
		DateTimeFormat df = DateTimeFormat.getFormat("yyyy MMM dd");

		IProject project = ProjectJsonConverter.getInstance().makeProject();
		project.setName(projectName);
		project.setStartDate(projectDateDebut);
		project.setEndDate(projectDateFin);
		project.setDescription(projectDesc);
		String projectJson = ProjectJsonConverter.getInstance().serializeToJson(project);
		rb.setRequestData(projectJson);

		rb.setCallback(new RequestCallback() {
			public void onResponseReceived(Request req, Response res) {
				if (res.getStatusCode() == 200) {
					loadAllProjects();
				}
			}

			public void onError(Request request, Throwable exception) {
				Window.alert("Error while adding the project");
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while adding the project");
		}
	}

	protected void addSprint(String sprintName, Date projectDateDebut, Date projectDateFin, String sprintComment,
			final IProject project) {

		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST,
				URL.encode(URL_REST + "sprints/add/" + project.getId()));
		rb.setHeader("Content-Type", "application/json");
		DateTimeFormat df = DateTimeFormat.getFormat("yyyy MMM dd");

		ISprint sprint = SprintJsonConverter.getInstance().makeSprint();
		sprint.setName(sprintName);
		sprint.setStartDate(projectDateDebut);
		sprint.setEndDate(projectDateFin);
		sprint.setComment(sprintComment);
		String sprintJson = SprintJsonConverter.getInstance().serializeToJson(sprint);
		rb.setRequestData(sprintJson);

		rb.setCallback(new RequestCallback() {
			public void onResponseReceived(Request req, Response res) {
				if (res.getStatusCode() == 200) {
					loadProjectSprints(project);
				}
			}

			public void onError(Request request, Throwable exception) {
				Window.alert("Error while adding the sprint");
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while adding the sprint");
		}
	}

	protected void addTask(String taskName, Date taskDateDebut, Date taskDateFin, String taskComment,
			final ISprint sprint) {

		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST,
				URL.encode(URL_REST + "tasks/add/" + sprint.getId()));
		rb.setHeader("Content-Type", "application/json");
		DateTimeFormat df = DateTimeFormat.getFormat("yyyy MMM dd");

		ITask task = TaskJsonConverter.getInstance().makeTask();
		task.setName(taskName);
		task.setStartDate(taskDateDebut);
		task.setDeadline(taskDateFin);
		task.setComment(taskComment);
		String taskJson = TaskJsonConverter.getInstance().serializeToJson(task);
		rb.setRequestData(taskJson);

		rb.setCallback(new RequestCallback() {
			public void onResponseReceived(Request req, Response res) {
				if (res.getStatusCode() == 200) {
					loadSprintTasks(sprint);
				}
			}

			public void onError(Request request, Throwable exception) {
				Window.alert("Error while adding the task");
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while adding the task");
		}
	}

	protected void updateTask(int id, String taskName, Date taskDateDebut, Date taskDateFin, String taskComment,
			String etat, final ISprint sprint) {

		RequestBuilder rb = new RequestBuilder(RequestBuilder.PUT, URL.encode(URL_REST + "tasks/update"));
		rb.setHeader("Content-Type", "application/json");
		DateTimeFormat df = DateTimeFormat.getFormat("yyyy MMM dd");

		ITask task = TaskJsonConverter.getInstance().makeTask();
		task.setId(id);
		task.setName(taskName);
		task.setStartDate(taskDateDebut);
		task.setDeadline(taskDateFin);
		task.setComment(taskComment);
		task.setEtat(etat);
		task.setSprint(sprint);
		String taskJson = TaskJsonConverter.getInstance().serializeToJson(task);
		rb.setRequestData(taskJson);

		rb.setCallback(new RequestCallback() {
			public void onResponseReceived(Request req, Response res) {
				if (res.getStatusCode() == 200) {
					loadSprintTasks(sprint);
				}
			}

			public void onError(Request request, Throwable exception) {
				Window.alert("Error while updating the task");
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while updating the task");
		}
	}
	
	protected void updateSprint(int id, String sprintName, Date sprintDateDebut, Date sprintDateFin, String sprintComment, final IProject project) {

		RequestBuilder rb = new RequestBuilder(RequestBuilder.PUT, URL.encode(URL_REST + "sprints/update"));
		rb.setHeader("Content-Type", "application/json");
		DateTimeFormat df = DateTimeFormat.getFormat("yyyy MMM dd");

		ISprint sprint = SprintJsonConverter.getInstance().makeSprint();
		sprint.setId(id);
		sprint.setName(sprintName);
		sprint.setStartDate(sprintDateDebut);
		sprint.setEndDate(sprintDateFin);
		sprint.setComment(sprintComment);
		sprint.setProject(project); 
		String sprintJson = SprintJsonConverter.getInstance().serializeToJson(sprint);
		rb.setRequestData(sprintJson);

		rb.setCallback(new RequestCallback() {
			public void onResponseReceived(Request req, Response res) {
				if (res.getStatusCode() == 200) {
					loadProjectSprints(project);
				}
			}

			public void onError(Request request, Throwable exception) {
				Window.alert("Error while updating the sprint");
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while updating the sprint");
		}
	}

}
