package fr.istic.taa.gwt.shared;

import java.util.List;

/*
 * { "projects": [ ... ] }
 */

public interface IProjects {
	
	void setProjects(List<IProject> projects);
	
	List<IProject> getProjects();
	
}