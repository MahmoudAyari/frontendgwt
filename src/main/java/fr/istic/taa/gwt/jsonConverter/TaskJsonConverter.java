package fr.istic.taa.gwt.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.taa.gwt.shared.ISprint;
import fr.istic.taa.gwt.shared.ITask;
import fr.istic.taa.gwt.shared.JsonFactory;






public class TaskJsonConverter {

	private TaskJsonConverter() {

	}

	private static TaskJsonConverter instance = new TaskJsonConverter();


	// Instantiate the factory
	JsonFactory factory = GWT.create(JsonFactory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public ITask makeTask() {
		// Construct the AutoBean
		AutoBean<ITask> task = factory.task();

		// Return the Book interface shim
		return task.as();
	}

	public String serializeToJson(ITask task) {
		// Retrieve the AutoBean controller
		AutoBean<ITask> bean = AutoBeanUtils.getAutoBean(task);

		return AutoBeanCodex.encode(bean).getPayload();
	}

	public ITask deserializeFromJson(String json) {
		AutoBean<ITask> bean = AutoBeanCodex.decode(factory, ITask.class, json);
		return bean.as();
	}

	public static TaskJsonConverter getInstance() {
		return instance;
	}
}
