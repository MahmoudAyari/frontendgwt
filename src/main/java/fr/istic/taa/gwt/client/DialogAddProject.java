package fr.istic.taa.gwt.client;

import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;

public class DialogAddProject extends DialogBox {

	Agile view;

	public DialogAddProject(Agile agile) {
		
		this.view = agile;

		/* Title of the dialog box */
		setText("Ajout d'un projet");

		setAnimationEnabled(true);
		setGlassEnabled(true);
		setAutoHideEnabled(true);

		/* Place field */
		Label nameLabel = new Label("Nom");
		final TextBox tName = new TextBox();
		tName.setWidth("120px");
		HorizontalPanel namePanel = new HorizontalPanel();
		namePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		namePanel.setSpacing(3);
		namePanel.add(nameLabel);
		namePanel.add(tName);

		/* Date fields */
		
		Label datedebutLabel = new Label("Date début");		
		final DateBox datedebut = new DateBox();
		datedebut.setWidth("100px");
		HorizontalPanel datedebutPanel = new HorizontalPanel();
		datedebutPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		datedebutPanel.setSpacing(3);
		datedebutPanel.add(datedebutLabel);
		datedebutPanel.add(datedebut);
		
		Label datefinLabel = new Label("Date fin");		
		final DateBox datefin = new DateBox();
		datedebut.setWidth("100px");
		HorizontalPanel datefinPanel = new HorizontalPanel();
		datefinPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		datefinPanel.setSpacing(3);
		datefinPanel.add(datefinLabel);
		datefinPanel.add(datefin);

		/* Description */
		Label descLabel = new Label("Description");
		final TextArea tDesc = new TextArea();
		tDesc.setWidth("150px");
		tDesc.setHeight("45px");
		HorizontalPanel descPanel = new HorizontalPanel();
		descPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		descPanel.setSpacing(3);
		descPanel.add(descLabel);
		descPanel.add(tDesc);

		/* Add all in a vertical panel */
		VerticalPanel panel = new VerticalPanel();
		panel.setHeight("100");
		panel.setWidth("300");
		panel.setSpacing(10);
		panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		panel.add(namePanel);
		panel.add(datedebutPanel);
		panel.add(datefinPanel);
		panel.add(descPanel);


		/* vaider button */
		Button valider = new Button("Valider");
		valider.setStyleName("btn");
		valider.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String projectName = tName.getText().trim();
				Date projectDateDebut = datedebut.getValue();
				Date projectDateFin = datefin.getValue();
				String projectDesc = tDesc.getText().trim();				
				if (projectName == "") {
					Window.alert("Entrez un nom");
				} else if (projectDateDebut == null) {
					Window.alert("Entrez une date début");
				} else if (projectDateFin == null) {
					Window.alert("Entrez une date fin");
				} else if (projectDesc == "") {
					Window.alert("Entrez une description correcte");
				} else {
					view.addProject(projectName, projectDateDebut, projectDateFin,projectDesc);
					DialogAddProject.this.hide();
				}          
			}
		});

		Button annuler = new Button("Annuler");
		annuler.setStyleName("btn");
		annuler.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				DialogAddProject.this.hide();      
			}
		});
		
		HorizontalPanel hp = new HorizontalPanel();
	    hp.setCellHorizontalAlignment(valider, HasHorizontalAlignment.ALIGN_RIGHT);
	    hp.setCellHorizontalAlignment(annuler, HasHorizontalAlignment.ALIGN_RIGHT);
	    hp.add(valider);
	    hp.add(annuler);  
	    
		/* Add validate button */
		panel.add(hp); 

		setWidget(panel);
	}



}
