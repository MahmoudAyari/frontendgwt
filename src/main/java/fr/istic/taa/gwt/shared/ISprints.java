package fr.istic.taa.gwt.shared;

import java.util.List;

public interface ISprints {
	
    void setSprints(List<ISprint> sprints);
	
	List<ISprint> getSprints();
}
