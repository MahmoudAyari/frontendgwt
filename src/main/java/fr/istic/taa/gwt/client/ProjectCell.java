package fr.istic.taa.gwt.client;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.istic.taa.gwt.shared.IProject;

/**
 * Renders a Cell which represents a project, in the projects list
 */
public class ProjectCell extends AbstractCell<IProject> {

	/**
	 * The HTML templates used to render a Cell representing a project
	 */
	interface Templates extends SafeHtmlTemplates {		
		@SafeHtmlTemplates.Template("<div>"
								  + 	"<span class='project-name'>{0}</span> - <span>{1}</span>"
								  + "</div>"
								  + "<div class='project-desc'>{2}</div>")
		SafeHtml makeProjectCell(String name, String dateStart, String desc);		
	}

	/**
	 * Create a singleton instance of the templates used to render the cell.
	 */
	private static Templates templates = GWT.create(Templates.class);

	@Override
	public void render(Context context, IProject value, SafeHtmlBuilder sb) {
		if (value != null) {
			// Use the template to create the HTML of an event Cell
			String dateStart = DateTimeFormat.getFormat("dd/MM/yyyy").format(value.getStartDate());
			String dateEnd = DateTimeFormat.getFormat("dd/MM/yyyy").format(value.getEndDate());
			SafeHtml project_cell_rendered = templates.makeProjectCell(value.getName(), dateStart, value.getDescription());
			sb.append(project_cell_rendered);
		}		
	}
}