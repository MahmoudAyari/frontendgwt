package fr.istic.taa.gwt.shared;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;

public interface JsonFactory extends AutoBeanFactory {
	
  AutoBean<IProject> project();
  
  AutoBean<IProjects> projects();

  AutoBean<ISprints> sprints();
  
  AutoBean<ISprint> sprint();
  
  AutoBean<ITask> task();
  
  AutoBean<ITasks> tasks();
  
}