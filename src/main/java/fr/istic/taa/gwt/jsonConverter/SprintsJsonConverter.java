package fr.istic.taa.gwt.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.taa.gwt.shared.ISprints;
import fr.istic.taa.gwt.shared.JsonFactory;



public class SprintsJsonConverter {

	private SprintsJsonConverter() {

	}

	private static SprintsJsonConverter instance = new SprintsJsonConverter();


	// Instantiate the factory
	JsonFactory factory = GWT.create(JsonFactory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public ISprints makeProjects() {
		// Construct the AutoBean
		AutoBean<ISprints> sprints = factory.sprints();

		// Return the Book interface shim
		return sprints.as();
	}

	String serializeToJson(ISprints sprints) {
		// Retrieve the AutoBean controller
		AutoBean<ISprints> bean = AutoBeanUtils.getAutoBean(sprints);

		return AutoBeanCodex.encode(bean).getPayload();
	}

	public ISprints deserializeFromJson(String json) {
		AutoBean<ISprints> bean = AutoBeanCodex.decode(factory, ISprints.class,"{\"sprints\":" + json + "}");
		return bean.as();
	}

	public static SprintsJsonConverter getInstance() {
		return instance;
	}
}
